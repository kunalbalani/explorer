//
//  ATCommandResponse.h
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/14/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ATRoom;
@class ATCommand;

@interface ATCommandResponse : NSObject

@property (nonatomic, strong) ATCommand *command;
@property (nonatomic, copy) NSArray<ATRoom *> *connections;
@property (nonatomic, copy) NSNumber *order;
@property (nonatomic, copy) NSString *writing;

@end
