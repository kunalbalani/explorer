//  ATRoom.h
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ATRoom : NSObject

- (instancetype)initWithRoomID:(NSString *)roomID;

@property (nonatomic, copy , readonly) NSString *roomID;
@property (nonatomic, copy) NSArray <NSString *> *writings;
@property (nonatomic, assign) BOOL isRoomExplored;

@end
