//
//  ViewController.m
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import "ATCommandViewController.h"
#import "UIColor+ATColor.h"
#import "ATStatusTextView.h"
#import "ATCommandButton.h"
#import "ATCommandDataController.h"
#import "ATExploreOperation.h"

@interface ATCommandViewController () <ATExploreOperationDelegate>
@property (nonatomic, copy) NSString *previousMessage;
// views
@property (weak, nonatomic) UIButton *startButton;
@property (weak, nonatomic) UIButton *reportButton;
@property (weak, nonatomic) ATStatusTextView *statusMessageTextView;

@property (nonatomic, strong) ATExploreOperation *exploreOperation;
@property (nonatomic, strong) ATReport *report;
@end

@implementation ATCommandViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];
    [self setupConstraints];
}

#pragma mark - UI

- (void)setupUI
{
    UIButton *start = [ATCommandButton new];
    start.translatesAutoresizingMaskIntoConstraints = NO;
    [start setTitle:@"Start Exploring" forState:UIControlStateNormal];
    [start addTarget:self
                  action:@selector(startButtonPressed:)
        forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:start];
    self.startButton = start;

    UIButton *report = [ATCommandButton new];
    report.translatesAutoresizingMaskIntoConstraints = NO;
    [report setTitle:@"Report" forState:UIControlStateNormal];
    [report addTarget:self
                  action:@selector(reportButtonPressed:)
        forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:report];
    self.reportButton = report;

    ATStatusTextView *statusView = [ATStatusTextView new];
    statusView.translatesAutoresizingMaskIntoConstraints = NO;
    [statusView logInfo:@"Hit Start Exploring..."];

    [self.view addSubview:statusView];
    self.statusMessageTextView = statusView;

    self.view.backgroundColor = [UIColor colorWithImage:@"space_image"];
}

- (void)setupConstraints
{
    NSDictionary *views =
        NSDictionaryOfVariableBindings(_startButton, _reportButton, _statusMessageTextView);
    NSDictionary *metrics = @{ @"buttonHeight" : @50.0, @"screenVerticalPadding" : @40.0 };

    [self.view addConstraints:[NSLayoutConstraint
                                  constraintsWithVisualFormat:@"H:|-[_startButton]-|"
                                                      options:NSLayoutFormatAlignAllBaseline
                                                      metrics:nil
                                                        views:views]];
    [self.view addConstraints:[NSLayoutConstraint
                                  constraintsWithVisualFormat:@"H:|-[_statusMessageTextView]-|"
                                                      options:NSLayoutFormatAlignAllBaseline
                                                      metrics:metrics
                                                        views:views]];

    [self.view addConstraints:[NSLayoutConstraint
                                  constraintsWithVisualFormat:@"H:|-[_reportButton]-|"
                                                      options:NSLayoutFormatAlignAllBaseline
                                                      metrics:metrics
                                                        views:views]];

    [self.view
        addConstraints:[NSLayoutConstraint
                           constraintsWithVisualFormat:@"V:|-screenVerticalPadding-[_startButton("
                                                       @"buttonHeight)]-[_statusMessageTextView]-["
                                                       @"_reportButton(buttonHeight)]-"
                                                       @"screenVerticalPadding-|"
                                               options:0
                                               metrics:metrics
                                                 views:views]];
}

#pragma mark - Actions

- (void)startButtonPressed:(id)sender
{
    if (self.exploreOperation.isExecuting)
        return;

    __weak ATCommandViewController *weakSelf = self;

    [[ATCommandDataController sharedInstance]
        startExploringWithCompletion:^(ATRoom *_Nullable startRoom,
                                       NSArray<ATDrone *> *_Nullable drones,
                                       NSError *_Nullable error) {
          if (!error)
          {
              dispatch_async(dispatch_get_main_queue(), ^{
                NSString *infoMessage =
                    [NSString stringWithFormat:@"Found %lu drones", [drones count]];
                [weakSelf.statusMessageTextView logInfo:infoMessage];
              });

              weakSelf.exploreOperation =
                  [[ATExploreOperation alloc] initWithRoom:startRoom drones:drones];
              weakSelf.exploreOperation.delegate = self;
              [[NSOperationQueue mainQueue] addOperation:weakSelf.exploreOperation];
          }
          else
          {
              dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.statusMessageTextView logError:[error localizedDescription]];
              });
          }
        }];
}

- (void)reportButtonPressed:(id)sender
{
    if (self.exploreOperation.isExecuting)
    {
        [self.statusMessageTextView logError:@"Operation is still executing.."];
        return;
    }
    [[ATCommandDataController sharedInstance]
        sendReport:self.report
        completion:^(BOOL success, NSError *_Nonnull error) {
          if (!error)
          {
              [self.statusMessageTextView logMessage:@"Message successfully reported"];
          }
          else
          {
              [self.statusMessageTextView logError:[error localizedDescription]];
          }
        }];
}

#pragma mark - ATExploreOpeationDelegate

- (void)didStartExploration { [self.statusMessageTextView logInfo:@"Exploration started.."]; }
- (void)didFinishExploration:(ATReport *)report
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.statusMessageTextView logInfo:@"Exploration completed"];
    });
    self.report = report;
}

- (void)messageUpdated:(ATReport *)report
{
    NSString *message = [report messageSoFar];
    if ([message length] > 0)
    {
        NSString *progressInfo = [NSString stringWithFormat:@"message so far : %@", message];
        if ([progressInfo isEqualToString:self.previousMessage])
        {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
          [self.statusMessageTextView logMessage:progressInfo];
          self.previousMessage = progressInfo;
        });
    }
}

@end
