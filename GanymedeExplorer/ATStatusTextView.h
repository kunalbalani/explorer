//
//  ATStatusTextView.h
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ATStatusTextView : UITextView

/**
 Displays log message in default color
 */
- (void)logInfo:(NSString *)info;

/**
 Displays success message in Green color
 */
- (void)logMessage:(NSString *)message;

/**
 Displays error message in Red color
 */
- (void)logError:(NSString *)error;

@end
