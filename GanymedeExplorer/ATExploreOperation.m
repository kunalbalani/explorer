//
//  ATExploreOperation.m
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/14/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import "ATExploreOperation.h"
#import "ATModels.h"
#import "ATCommandDataController.h"

NSUInteger const kCommandsBatchSize = 5;

@interface ATExploreOperation ()

@property (atomic, assign) BOOL _executing;
@property (atomic, assign) BOOL _finished;

@property (nonatomic, assign) NSUInteger nextAvailableDroneIndex;
@property (nonatomic, strong) NSArray<ATDrone *> *allDrones;
// used to avoid any cycles
@property (nonatomic, strong) NSMutableArray<ATRoom *> *allRooms;
@property (nonatomic, strong) ATReport *report;
// dispatcher
@property (nonatomic, strong) NSMutableArray<ATCommand *> *commandsQueue;

@end

@implementation ATExploreOperation

- (instancetype)initWithRoom:(ATRoom *)room drones:(NSArray<ATDrone *> *)drones
{
    self = [super init];
    if (self)
    {
        _allDrones = drones;
        _allRooms = [@[ room ] mutableCopy];
        _commandsQueue = [NSMutableArray array];
        _report = [[ATReport alloc] init];
        _nextAvailableDroneIndex = 0;
    }
    return self;
}

- (void)start;
{
    if ([self isCancelled])
    {
        // Move the operation to the finished state if it is canceled.
        [self willChangeValueForKey:@"isFinished"];
        self._finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }

    // If the operation is not canceled, begin executing the task.
    [self willChangeValueForKey:@"isExecuting"];
    self._executing = YES;
    [self didChangeValueForKey:@"isExecuting"];

    [self.delegate didStartExploration];
    [self exploreRoom:self.allRooms[0]];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self processNextSetOfCommands];
    });
}

// Uses recursive DFS algorithm
- (void)exploreRoom:(ATRoom *)room
{
    if (!room.isRoomExplored)
    {
        // explore other rooms
        ATCommand *exploreCommand = [[ATCommand alloc] init];
        exploreCommand.associatedRoom = room;
        exploreCommand.commandType = ATCommandExplore;
        [self dispatchCommand:exploreCommand];

        // read
        ATCommand *readCommand = [[ATCommand alloc] init];
        readCommand.associatedRoom = room;
        readCommand.commandType = ATCommandRead;
        [self dispatchCommand:readCommand];
    }
}

- (void)dispatchCommand:(ATCommand *)command
{
    @synchronized(self.commandsQueue) { [self.commandsQueue addObject:command]; }
}

- (void)processNextSetOfCommands
{
    NSMutableArray *commands = [NSMutableArray arrayWithCapacity:kCommandsBatchSize];
    NSUInteger totalCommands = [self.commandsQueue count] > kCommandsBatchSize
                                   ? kCommandsBatchSize
                                   : [self.commandsQueue count];

    @synchronized(self.commandsQueue)
    {
        for (int i = 0; i < totalCommands; i++)
        {
            [commands addObject:self.commandsQueue[i]];
        }
    }
    if ([commands count] == 0) // no more commands in queue :)
    {
        [self completeOperation];
        return;
    }

    __weak ATExploreOperation *weakSelf = self;
    [[ATCommandDataController sharedInstance]
        issueCommands:commands
                drone:[self nextAvailableDrone]
           completion:^(NSArray<ATCommandResponse *> *_Nullable commands,
                        NSError *_Nullable error) {
             if (!error)
             {
                 for (ATCommandResponse *command in commands)
                 {
                     [weakSelf processCommandResponse:command];
                 }
             }
             [self processNextSetOfCommands];
           }];
}

- (void)processCommandResponse:(ATCommandResponse *)commandResponse
{
    @synchronized(self.commandsQueue) { [self.commandsQueue removeObject:commandResponse.command]; }
    if (commandResponse.command.commandType == ATCommandExplore)
    {
        for (ATRoom *connectingRoom in commandResponse.connections)
        {
            if ([self.allRooms containsObject:connectingRoom])
            {
                connectingRoom.isRoomExplored = YES;
            }
            else
            {
                [self.allRooms addObject:connectingRoom];
                [self exploreRoom:connectingRoom];
            }
        }
    }
    else
    {
        [self.report setWriting:commandResponse.writing order:commandResponse.order];
        [self.delegate messageUpdated:self.report];
    }
}

- (ATDrone *)nextAvailableDrone
{
    @synchronized(self.allDrones)
    {
        ATDrone *availableDrone = self.allDrones[self.nextAvailableDroneIndex];
        NSUInteger totalDrones = [self.allDrones count];
        self.nextAvailableDroneIndex = (self.nextAvailableDroneIndex + 1) % totalDrones;
        return availableDrone;
    }
}

#pragma mark - NSOperation Subclass

- (void)main;
{
    if ([self isCancelled])
    {
        return;
    }
}

- (BOOL)isAsynchronous;
{
    return YES;
}

- (BOOL)isExecuting { return self._executing; }
- (BOOL)isFinished { return self._finished; }
- (void)completeOperation
{
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];

    self._executing = NO;
    self._finished = YES;

    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];

    [self.delegate didFinishExploration:self.report];
}

@end