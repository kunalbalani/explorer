//
//  UIView+ATView.h
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ATView)

- (void)setRoundedCorners:(UIRectCorner)corners radius:(CGFloat)radius;

@end
