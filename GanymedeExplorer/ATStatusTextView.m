//
//  ATStatusTextView.m
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import "ATStatusTextView.h"
#import "UIView+ATView.h"

@interface ATStatusTextView () <UITextViewDelegate>

@end

@implementation ATStatusTextView

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.editable = NO;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self setupUI];
}

- (void)setupUI
{
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.layer.borderWidth = 2.0f;
    self.font = [UIFont fontWithName:@"Verdana" size:9];
    self.delegate = self;

    [self setRoundedCorners:UIRectCornerAllCorners radius:10];
    [self setBackgroundColor:[UIColor whiteColor]];
}

#pragma mark - Public methods

- (void)logError:(NSString *)error { [self appendString:error color:[UIColor redColor]]; }
- (void)logMessage:(NSString *)message { [self appendString:message color:[UIColor greenColor]]; }
- (void)logInfo:(NSString *)info { [self appendString:info color:[UIColor blackColor]]; }
#pragma mark - Helpers

- (void)appendString:(NSString *)string color:(UIColor *)color
{
    NSAttributedString *originalText = self.attributedText ?: [NSAttributedString new];
    NSMutableAttributedString *mutableAttributedString =
        [[NSMutableAttributedString alloc] initWithAttributedString:originalText];

    NSAttributedString *newString =
        [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ", string]
                                        attributes:@{NSForegroundColorAttributeName : color}];

    [mutableAttributedString appendAttributedString:newString];

    [self setAttributedText:mutableAttributedString];
}

@end
