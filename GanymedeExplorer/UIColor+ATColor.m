//
//  UIColor+ATColor.m
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import "UIColor+ATColor.h"

@implementation UIColor (ATColor)

+ (UIColor *)colorWithImage:(NSString *)imageName
{
    UIImage *image = [UIImage imageNamed:imageName];
    return [UIColor colorWithPatternImage:image];
}

@end
