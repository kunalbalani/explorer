//
//  ATRoom.m
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import "ATRoom.h"

@implementation ATRoom

- (instancetype)initWithRoomID:(NSString *)roomID
{
    self = [super init];
    if (self)
    {
        _roomID = roomID;
    }
    return self;
}

- (BOOL)isEqual:(id)object
{
    if (self == object)
    {
        return YES;
    }

    if (![object isKindOfClass:[ATRoom class]])
    {
        return NO;
    }

    return [self.roomID isEqualToString:[(ATRoom *)object roomID]];
}

- (NSString *)description
{
    return [NSString
        stringWithFormat:@"ATRoom description:%@\n roomID: %@\n", [super description], self.roomID];
}
@end
