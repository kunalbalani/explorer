//
//  ATReport.m
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import "ATReport.h"

@interface ATReport ()
@property (nonatomic, strong) NSMutableDictionary<NSNumber *, NSString *> *messages;
@end

@implementation ATReport

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _messages = [NSMutableDictionary new];
    }
    return self;
}

- (void)setWriting:(NSString *)writing order:(NSNumber *)order
{
    if ([writing length] > 0 && [order integerValue] != -1){
        [self.messages setObject:writing forKey:order];
    }
}

- (NSString *)messageSoFar
{
    NSMutableString *returnValue = [NSMutableString new];

    NSArray *sortedKeys = [[self.messages allKeys] sortedArrayUsingSelector:@selector(compare:)];
    for (NSNumber *key in sortedKeys)
        [returnValue appendString:[self.messages objectForKey:key]];

    return returnValue;
}

@end
