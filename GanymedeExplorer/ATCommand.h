//  ATCommand.h
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ATCommandResponse;
@class ATRoom;

typedef NS_ENUM(NSInteger, ATCommandType) { ATCommandExplore = 0, ATCommandRead };

@interface ATCommand : NSObject

@property (nonatomic, assign) ATCommandType commandType;
@property (nonatomic, copy, readonly) NSString *commandID;
@property (nonatomic, weak) ATCommandResponse *response;
@property (nonatomic, strong) ATRoom *associatedRoom;

- (NSDictionary *)dictinaryRepresentation;

@end
