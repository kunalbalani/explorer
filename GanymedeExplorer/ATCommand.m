//
//  ATCommand.m
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import "ATCommand.h"
#import "ATRoom.h"

@implementation ATCommand

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _commandID = [[self class] randomAlphanumericString];
    }
    return self;
}

- (NSDictionary *)dictinaryRepresentation
{
    if (self.commandType == ATCommandExplore)
    {
        return @{ @"explore" : self.associatedRoom.roomID };
    }
    else
    {
        return @{ @"read" : self.associatedRoom.roomID };
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"ATCommand : %@", self.commandID];
}

#pragma mark - HELPER

+ (NSString *)randomAlphanumericString
{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity:10];

    for (int i = 0; i < 10; i++)
    {
        [randomString
            appendFormat:@"%C", [letters characterAtIndex:arc4random() % [letters length]]];
    }

    return randomString;
}

@end
