//
//  ATExploreOperation.h
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/14/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ATRoom;
@class ATDrone;
@class ATReport;

@protocol ATExploreOperationDelegate <NSObject>

@required

/**
 called when operation is started
 **/
- (void)didStartExploration;

/**
 calls when exploring room
 **/
- (void)messageUpdated:(ATReport *)report;

/**
 called when operation is complete
 **/
- (void)didFinishExploration:(ATReport *)report;

@end

@interface ATExploreOperation : NSOperation

/**
 Initialize with intial room and set of drones

 @NOTE Operation needs to be started manually
 **/
- (instancetype)initWithRoom:(ATRoom *)room drones:(NSArray<ATDrone *> *)drones;

@property (nonatomic, weak) id<ATExploreOperationDelegate> delegate;

@end
