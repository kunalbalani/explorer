//
//  ATCommandDataController.h
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATModels.h"

NS_ASSUME_NONNULL_BEGIN

static NSString *const kCommanderEmailAddress = @"balani.kunal@gmail.com";

typedef void (^StartCompletionBlock)(ATRoom *_Nullable startRoom,
                                     NSArray<ATDrone *> *_Nullable drones,
                                     NSError *_Nullable error);
typedef void (^CommandCompletionBlock)(NSArray<ATCommandResponse *> *_Nullable commands,
                                       NSError *_Nullable error);

@interface ATCommandDataController : NSObject

/**
 Singleton accessor for data controller
 **/
+ (instancetype)sharedInstance;

/**
 Starts Exploring Labyrinths and returns initial room for exploration
 **/
- (void)startExploringWithCompletion:(StartCompletionBlock)startCompletionBlock;

/**
 Issues a command to the given drone. If failed returns nil with error.

 @NOTE: A given drone can only accept upto 5 commands
 **/
- (void)issueCommands:(NSArray<ATCommand *> *)commands
                drone:(ATDrone *)drone
           completion:(CommandCompletionBlock)commandCompletion;

/**
 Sends report to Commander.
 **/
- (void)sendReport:(ATReport *)report
        completion:(void (^__nullable)(BOOL success, NSError *error))completion;

@end

NS_ASSUME_NONNULL_END