//
//  ATCommandDataController.m
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import "ATCommandDataController.h"

NSString *const kCommanderEmailHTTPHeaderKey = @"x-commander-email";
NSString *const kCommanderBaseURL = @"http://challenge2.airtime.com:10001";

@interface ATCommandDataController ()
@property (nonatomic, strong) NSURLSession *session;
@end

@implementation ATCommandDataController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        [config setHTTPAdditionalHeaders:@{kCommanderEmailHTTPHeaderKey : kCommanderEmailAddress}];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        _session = session;
    }
    return self;
}

+ (instancetype)sharedInstance
{
    static ATCommandDataController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
      sharedInstance = [[ATCommandDataController alloc] init];
    });
    return sharedInstance;
}

/**
 Starts Exploring Labyrinths and returns initial room for exploration
 **/
- (void)startExploringWithCompletion:(StartCompletionBlock)startCompletionBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@/start", kCommanderBaseURL];
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];

    NSURLSessionDataTask *task = [self.session
        dataTaskWithRequest:request
          completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error)
            {
                NSDictionary *jsonDict =
                    [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

                ATRoom *room = [[ATRoom alloc] initWithRoomID:jsonDict[@"roomId"]];
                NSMutableArray *drones = [NSMutableArray array];
                for (NSString *droneIds in jsonDict[@"drones"])
                {
                    ATDrone *drone = [[ATDrone alloc] initWithDroneID:droneIds];
                    [drones addObject:drone];
                }
                startCompletionBlock(room, drones, error);
            }
            else
            {
                startCompletionBlock(nil, nil, error);
            }
          }];

    [task resume];
}

/**
 Issues a command to the given drone. If failed returns nil with error.

 @NOTE: A given drone can only accept upto 5 commands
 **/
- (void)issueCommands:(NSArray<ATCommand *> *)commands
                drone:(ATDrone *)drone
           completion:(CommandCompletionBlock)commandCompletion
{
    NSString *urlString =
        [NSString stringWithFormat:@"%@/drone/%@/commands", kCommanderBaseURL, drone.droneID];
    NSURL *URL = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    NSMutableDictionary *postBody = [NSMutableDictionary dictionary];
    for (ATCommand *command in commands)
    {
        [postBody setObject:[command dictinaryRepresentation] forKey:command.commandID];
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postBody options:0 error:nil];
    [request setHTTPBody:jsonData];

    __weak ATCommandDataController *weakSelf = self;

    NSURLSessionDataTask *task = [self.session
        dataTaskWithRequest:request
          completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error)
            {
                NSDictionary *jsonDict =
                    [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

                NSMutableArray *commandResponses = [NSMutableArray array];
                for (id commandID in [jsonDict allKeys])
                {
                    id commandResponseDict = [jsonDict objectForKey:commandID];
                    ATCommand *command = [weakSelf commandInArray:commands withID:commandID];
                    ATCommandResponse *response = [[ATCommandResponse alloc] init];
                    response.command = command;

                    if (command.commandType == ATCommandExplore)
                    {
                        NSMutableArray *rooms = [NSMutableArray array];
                        NSArray *connections = commandResponseDict[@"connections"];
                        for (NSString *roomID in connections)
                        {
                            ATRoom *room = [[ATRoom alloc] initWithRoomID:roomID];
                            [rooms addObject:room];
                        }
                        response.connections = rooms;
                    }
                    else
                    {
                        response.writing = commandResponseDict[@"writing"];
                        response.order = commandResponseDict[@"order"];
                    }
                    [commandResponses addObject:response];
                }
                commandCompletion(commandResponses, error);
            }

            else
            {
                commandCompletion(nil, error);
            }
          }];

    [task resume];
}

- (ATCommand *)commandInArray:(NSArray<ATCommand *> *)commands withID:(NSString *)commandID
{
    for (ATCommand *command in commands)
    {
        if ([command.commandID isEqualToString:commandID])
        {
            return command;
        }
    }
    return nil;
}

/**
 Sends report to Commander.
 **/
- (void)sendReport:(ATReport *)report
        completion:(void (^__nullable)(BOOL success, NSError *error))completion
{
    NSString *urlString = [NSString stringWithFormat:@"%@/report", kCommanderBaseURL];
    NSURL *URL = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    NSDictionary *postBody = @{ @"message" : report.messageSoFar };
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postBody options:0 error:nil];
    [request setHTTPBody:jsonData];

    NSURLSessionDataTask *task =
        [self.session dataTaskWithRequest:request
                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                          if (!error)
                          {
                              completion(YES, nil);
                          }
                          else
                          {
                              completion(NO, error);
                          }
                        }];

    [task resume];
}

@end
