//
//  ATDrone.h
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ATDrone : NSObject

/**
 Initialize instance with unique drone ID
 **/
- (instancetype)initWithDroneID:(NSString *)droneID;

@property (nonatomic, copy, readonly) NSString *droneID;

@end
