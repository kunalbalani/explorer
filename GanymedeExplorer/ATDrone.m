//
//  ATDrone.m
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import "ATDrone.h"

@implementation ATDrone

- (instancetype)initWithDroneID:(NSString *)droneID;
{
    self = [super init];
    if (self)
    {
        _droneID = droneID;
    }
    return self;
}

@end
