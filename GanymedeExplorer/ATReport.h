//
//  ATReport.h
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ATReport : NSObject


- (void)setWriting:(NSString *)writing order:(NSNumber *)order;

/**
 Constructs message in order from the parts
 **/
- (NSString *)messageSoFar;

@end
