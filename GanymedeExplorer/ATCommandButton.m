//
//  ATCommandButton.m
//  GanymedeExplorer
//
//  Created by Kunal Balani on 8/13/16.
//  Copyright © 2016 Airtime. All rights reserved.
//

#import "ATCommandButton.h"
#import "UIView+ATView.h"
#import "UIColor+ATColor.h"

@implementation ATCommandButton

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self setupUI];
}

- (void)setupUI
{
    self.titleLabel.font = [UIFont fontWithName:@"Verdana" size:24];
    self.titleLabel.tintColor = [UIColor lightTextColor];
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 2.0f;
    
    self.backgroundColor = [UIColor colorWithImage:@"button-bg"];
    [self setRoundedCorners:UIRectCornerAllCorners radius:10];
}

@end
